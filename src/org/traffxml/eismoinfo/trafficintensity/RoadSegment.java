/*
 * Copyright © 2019 traffxml.org.
 * 
 * This file is part of the traffxml-eismoinfo-intensity library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.eismoinfo.trafficintensity;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A stretch of road for which traffic is measured.
 * 
 * <p>A {@code RoadSegment} always refers to a single direction.
 * 
 * <p>Start and end coordinates always refer to the positive direction: a driver traveling in the negative
 * direction would go from {@link #endX}, {@link #endY} to {@link #startX}, {@link #startY}.
 * 
 * Coordinates are in LKS94 and can be converted using {@link Lks94}.
 */
public class RoadSegment {
	public final Double startX;
	public final Double startY;
	public final Double endX;
	public final Double endY;

	/**
	 * Direction of the road segment.
	 * 
	 * <p>Can be “positive” or “negative”.
	 */
	public final String direction;

	/** Number of vehicles currently on the segment. */
	public final Integer numberOfVehicles;

	/**
	 * Traffic intensity in words.
	 * 
	 * <p>Can be “normal”, “heavy” or “slow”.
	 */
	public final String trafficType;

	/**
	 * Average speed of vehicles on this segment.
	 * 
	 * <p>This value can be null, e.g. if {@link #numberOfVehicles} is zero.
	 */
	public final Double averageSpeed;

	/** Speed limit in summer, in km/h. */
	public final Integer summerSpeed;

	/** Speed limit in winter, in km/h. */
	public final Integer winterSpeed;

	static RoadSegment fromJson(JSONObject object) {
		Double startX = null;
		Double startY = null;
		Double endX = null;
		Double endY = null;
		String direction = null;
		Integer numberOfVehicles = null;
		String trafficType = null;
		Double averageSpeed = null;
		Integer summerSpeed = null;
		Integer winterSpeed = null;

		try {
			startX = object.getDouble("startX");
		} catch (JSONException e) {
			// NOP
		}
		try {
			startY = object.getDouble("startY");
		} catch (JSONException e) {
			// NOP
		}
		try {
			endX = object.getDouble("endX");
		} catch (JSONException e) {
			// NOP
		}
		try {
			endY = object.getDouble("endY");
		} catch (JSONException e) {
			// NOP
		}
		direction = object.optString("direction", null);
		try {
			numberOfVehicles = object.getInt("numberOfVehicles");
		} catch (JSONException e) {
			// NOP
		}
		trafficType = object.optString("trafficType", null);
		try {
			averageSpeed = object.getDouble("averageSpeed");
		} catch (JSONException e) {
			// NOP
		}
		try {
			summerSpeed = object.getInt("summerSpeed");
		} catch (JSONException e) {
			// NOP
		}
		try {
			winterSpeed = object.getInt("winterSpeed");
		} catch (JSONException e) {
			// NOP
		}

		return new RoadSegment(startX, startY, endX, endY, direction, numberOfVehicles, trafficType,
				averageSpeed, summerSpeed, winterSpeed);
	}

	public RoadSegment(Double startX, Double startY, Double endX, Double endY, String direction,
			Integer numberOfVehicles, String trafficType, Double averageSpeed, Integer summerSpeed,
			Integer winterSpeed) {
		super();
		this.startX = startX;
		this.startY = startY;
		this.endX = endX;
		this.endY = endY;
		this.direction = direction;
		this.numberOfVehicles = numberOfVehicles;
		this.trafficType = trafficType;
		this.averageSpeed = averageSpeed;
		this.summerSpeed = summerSpeed;
		this.winterSpeed = winterSpeed;
	}
}
