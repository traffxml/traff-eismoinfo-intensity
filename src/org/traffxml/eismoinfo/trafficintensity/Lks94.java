/*
 * Copyright © 2019 traffxml.org.
 * 
 * This file is part of the traffxml-eismoinfo-intensity library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.eismoinfo.trafficintensity;

import org.osgeo.proj4j.CRSFactory;
import org.osgeo.proj4j.CoordinateTransform;
import org.osgeo.proj4j.CoordinateTransformFactory;
import org.osgeo.proj4j.ProjCoordinate;
import org.traffxml.traff.LatLon;

/**
 * Support for the LKS94 coordinate system.
 * 
 * <p>LKS94 (EPSG:3346) is the official coordinate system in Lithuania. Easting and northing are in meters.
 * (500000, 0) corresponds to 0° N, 24° E.
 */
public class Lks94 {
	private static final CoordinateTransform TRANSFORM;

	static {
		CRSFactory crsFactory = new CRSFactory();
		CoordinateTransformFactory f = new CoordinateTransformFactory();
		TRANSFORM = f.createTransform(crsFactory.createFromName("EPSG:3346"),
				crsFactory.createFromName("EPSG:4326"));
	}

	/**
	 * Converts a coordinate pair from LKS94 to WGS84.
	 * 
	 * @param x Easting in LKS94
	 * @param y Northing in LKS94
	 * @return A WGS84 coordinate pair
	 */
	public static LatLon getWgs84FromLks94(double x, double y) {
		ProjCoordinate coord = new ProjCoordinate(x, y);
		ProjCoordinate target = new ProjCoordinate();

		TRANSFORM.transform(coord, target);
		return new LatLon((float) target.y, (float) target.x);
	}

}
