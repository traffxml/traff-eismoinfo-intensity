/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml-eismoinfo-intensity library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.eismoinfo.trafficintensity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.traffxml.traff.TraffMessage;

public class LtIntensityFeed {
	/**
	 * The logger for log output.
	 * 
	 * <p>Where messages will be logged depends on the slf4j binding supplied at runtime.
	 */
	static final Logger LOG = LoggerFactory.getLogger(Thread.currentThread().getStackTrace()[2].getClassName());

	public final LtIntensityMessage[] messages;

	public static LtIntensityFeed parseJson(InputStream stream) {
		List<LtIntensityMessage> messages = new ArrayList<LtIntensityMessage>();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		int i;

		try {
			i = stream.read();
			while (i != -1) {
				if (i >= 32) outputStream.write(i);
				i = stream.read();
			}
			stream.close();
		} catch (IOException e) {
			LOG.debug("{}", e);
			System.exit(1);
		}

		JSONTokener tokener = new JSONTokener(outputStream.toString());
		while (true) {
			Object object = null;
			try {
				object = tokener.nextValue();
			} catch (JSONException e) {
				LOG.debug("{}", e);
			}
			if (object == null)
				break;
			if (object instanceof JSONArray) {
				for (i = 0; i < ((JSONArray) object).length(); i++) {
					Object child;
					try {
						child = ((JSONArray) object).get(i);
					} catch (JSONException e) {
						LOG.warn("Failed to retrieve object #{} from top-level array", i);
						LOG.debug("{}", e);
						continue;
					}
					if (child instanceof JSONObject)
						messages.add(LtIntensityMessage.fromJson((JSONObject) child));
					else
						LOG.warn("Got instance of {} in top-level array, expected JSONObject",
								child.getClass().getName());
				}
			} else
				LOG.warn("Got instance of {} at top level, expected JSONArray",
						object.getClass().getName());
		}
		return new LtIntensityFeed(messages.toArray(new LtIntensityMessage[0]));
	}

	/**
	 * Converts the feed to a list of TraFF messages.
	 * 
	 * <p>This method allows previously received messages to be specified as an optional argument, in order for
	 * information from those messages to be used in the feed.
	 * 
	 * <p>Expiration time of messages can be influenced by the {@code pollInterval}: a message will be valid
	 * from its {@link LtIntensityMessage#date} for twice its {@link LtIntensityMessage#timeInterval}, or
	 * twice the {@code pollInterval} (whichever is longer), plus two minutes. This allows for one update to
	 * be missed without messages expiring. A {@code pollInterval} of zero has no effect.
	 * 
	 * @param oldMessages Previously received messages
	 * @param pollInterval Interval at which this source is being polled, in minutes
	 * @param returnNormal if true, all messages will be returned, else only those messages which indicate a
	 * congestion.
	 */
	public List<TraffMessage> toTraff(String sourcePrefix, Collection<TraffMessage> oldMessages, int pollInterval,
			boolean returnNormal) {
		Map<String, TraffMessage> outMap = new HashMap<String, TraffMessage>();
		for (LtIntensityMessage inMessage : messages)
			for (TraffMessage message : inMessage.toTraff(sourcePrefix, oldMessages, pollInterval, returnNormal))
				outMap.put(message.id, message);
		/* Add cancellations for all unmatched old messages */
		if (oldMessages != null)
			for (TraffMessage oldMessage : oldMessages)
				if (oldMessage.id.startsWith(sourcePrefix + ":")
						&& !outMap.containsKey(oldMessage.id)) {
					TraffMessage.Builder builder = new TraffMessage.Builder();
					builder.setId(oldMessage.id);
					builder.setReceiveTime(oldMessage.receiveTime);
					builder.setUpdateTime(new Date());
					Date expirationTime = oldMessage.expirationTime;
					if (expirationTime == null)
						expirationTime = oldMessage.endTime;
					builder.setExpirationTime(expirationTime);
					builder.setCancellation(true);
					outMap.put(oldMessage.id, builder.build());
				}
		List<TraffMessage> res = new ArrayList<TraffMessage>();
		res.addAll(outMap.values());
		return res;
	}

	public LtIntensityFeed(LtIntensityMessage[] messages) {
		super();
		this.messages = messages;
	}
}
