/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml-eismoinfo-intensity library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.eismoinfo.trafficintensity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.threeten.bp.Instant;
import org.traffxml.traff.BoundingBox;
import org.traffxml.traff.LatLon;
import org.traffxml.traff.TraffEvent;
import org.traffxml.traff.TraffLocation;
import org.traffxml.traff.TraffLocation.Directionality;
import org.traffxml.traff.TraffLocation.RoadClass;
import org.traffxml.traff.TraffMessage;

/**
 * Encapsulates a traffic measurement from a single station.
 * 
 * <p>Each instance has a unique numeric ID, a coordinate pair and a name, which presumably refer to the
 * measurement point.
 */
public class LtIntensityMessage {
	/**
	 * The logger for log output.
	 * 
	 * <p>Where messages will be logged depends on the slf4j binding supplied at runtime.
	 */
	static final Logger LOG = LoggerFactory.getLogger(Thread.currentThread().getStackTrace()[2].getClassName());

	static Map<String, Set<BoundingBox>> motorwayBboxes = new HashMap<String, Set<BoundingBox>>();

	static {
		/* A1: Klaipėda—Kaunas, Kaunas–Vilnius */
		Set<BoundingBox> set = new HashSet<BoundingBox>();
		set.add(new BoundingBox(54.9634422f, 21.2275298f, 55.7328223f, 23.8624136f));
		set.add(new BoundingBox(54.6841780f, 23.9994504f, 54.9309817f, 25.0560650f));
		motorwayBboxes.put("A1", set);

		/* Vilnius-Panevėžys */
		set = new HashSet<BoundingBox>();
		set.add(new BoundingBox(54.7713607f, 24.3627749f, 55.6645785f, 25.1917797f));
		motorwayBboxes.put("A2", set);

		/* Marijampolė–Kaunas */
		set = new HashSet<BoundingBox>();
		set.add(new BoundingBox(54.5973773f, 23.3969578f, 54.7807138f, 23.7791276f));
		motorwayBboxes.put("A5", set);

		/* Radviliškis–Šiauliai */
		set = new HashSet<BoundingBox>();
		set.add(new BoundingBox(55.8442342f, 23.4012382f, 55.9184609f, 23.5009620f));
		motorwayBboxes.put("A9", set);
	}

	static Map<String, TraffLocation.RoadClass> defaultRoadClassMappings = new HashMap<String, TraffLocation.RoadClass>();

	static {
		defaultRoadClassMappings.put("A1", RoadClass.MOTORWAY);
		defaultRoadClassMappings.put("A2", RoadClass.MOTORWAY);
		defaultRoadClassMappings.put("A5", RoadClass.TRUNK);
		defaultRoadClassMappings.put("A9", RoadClass.TRUNK);
	}

	/** Timestamp at which the data was last updated. */
	public final Date date;

	/** Unique ID for the measurement station. */
	public final Integer id;

	/** Road reference number. */
	public final String roadNr;

	public final Double km;

	/** Road name, usually a route description (asterisks denote locations outside Lithuania). */
	public final String roadName;

	/** Latitude (note that this determines the y coordinate on screen). */
	public final Double x;

	/** Longitude  (note that this determines the x coordinate on screen). */
	public final Double y;

	/** Name of the measurement station, usually a nearby place or landmark. */
	public final String name;

	/**
	 * Update interval in minutes.
	 * 
	 * <p>Values of 14, 15 or 16 are common; updates always seem to happen within the first two minutes
	 * following a quarter-hour. Some sources update every minute.
	 */
	public final Integer timeInterval;

	/**
	 * Road segments measured.
	 * 
	 * <p>Usually two members, one for each direction.
	 */
	public final RoadSegment[] roadSegments;

	static LtIntensityMessage fromJson(JSONObject object) {
		Date date = null;
		Integer id = null;
		String roadNr = null;
		Double km = null;
		String roadName = null;
		Double x = null;
		Double y = null;
		String name = null;
		Integer timeInterval = null;
		List<RoadSegment> roadSegments = null;

		try {
			date = new Date(object.getLong("date"));
		} catch (JSONException e) {
			try {
				date = parseDate(object.getString("date"));
			} catch (JSONException e2) {
				e.printStackTrace();
				// NOP
			}
		}
		try {
			id = object.getInt("id");
		} catch (JSONException e) {
			// NOP
		}
		roadNr = object.optString("roadNr", null);
		try {
			km = object.getDouble("km");
		} catch (JSONException e) {
			// NOP
		}
		roadName = object.optString("roadName", null);
		try {
			x = object.getDouble("x");
		} catch (JSONException e) {
			// NOP
		}
		try {
			y = object.getDouble("y");
		} catch (JSONException e) {
			// NOP
		}
		name = object.optString("name", null);
		try {
			timeInterval = object.getInt("timeInterval");
		} catch (JSONException e) {
			// NOP
		}
		try {
			JSONArray segArray = object.getJSONArray("roadSegments");
			roadSegments = new ArrayList<RoadSegment>(2);
			for (int i = 0; i < segArray.length(); i++) {
				Object child = segArray.get(i);
				if (child instanceof JSONObject) {
					roadSegments.add(RoadSegment.fromJson((JSONObject) child));
				} else
					LOG.warn("Got instance of {} for roadSegments, expected JSONArray",
							child.getClass().getName());
			}
		} catch (JSONException e) {
			// NOP
		}

		return new LtIntensityMessage(date, id, roadNr, km, roadName, x, y, name, timeInterval,
				(roadSegments == null) ? null : roadSegments.toArray(new RoadSegment[0]));
	}

	/**
	 * Parses a timestamp in ISO 8601 format.
	 * 
	 * @param iso8601 The timestamp
	 * @return A date corresponding to the timestamp supplied
	 */
	private static Date parseDate(String iso8601) {
		/* use a local variable because SimpleDateFormat#parse() is not thread-safe */
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.ROOT);
		if (iso8601 != null)
			try {
				return format.parse(iso8601);
			} catch (ParseException e) {
				LOG.warn("Not a valid ISO8601 timestamp: " + iso8601 + ", ignoring");
			}
		return null;
	}

	/**
	 * Converts the message to a list of TraFF messages.
	 * 
	 * <p>This method allows previously received messages to be specified as an optional argument. If the ID of
	 * the new message matches that of a message in the collection, the {@code receiveTime} of the old
	 * message is used. 
	 * 
	 * <p>Expiration time of messages can be influenced by the {@code pollInterval}: a message will be valid
	 * from its {@link LtIntensityMessage#date} for twice its {@link LtIntensityMessage#timeInterval} or
	 * twice the {@code pollInterval} (whichever is longer), plus two minutes. This allows for one update to
	 * be missed without messages expiring. A {@code pollInterval} of zero has no effect.
	 * 
	 * @param oldMessages Previously received messages
	 * @param pollInterval Interval at which this source is being polled, in minutes
	 * @param returnNormal if true, all messages will be returned, else only those messages which indicate a
	 * congestion (if traffic is normal on all segments, an empty list will be returned).
	 */
	public Collection<TraffMessage> toTraff(String sourcePrefix, Collection<TraffMessage> oldMessages, int pollInterval,
			boolean returnNormal) {
		List<TraffMessage> res = new ArrayList<TraffMessage>();
		for (RoadSegment segment : roadSegments) {
			String id = sourcePrefix + ":" + this.id + ","
					+ ("negative".equals(segment.direction) ? "n" : "p");
			TraffMessage.Builder builder = new TraffMessage.Builder();
			builder.setId(id);
			builder.setUpdateTime(date);
			Date receiveTime = date;
			boolean hasOldMessage = false;

			/* determine receive time and expiration time */
			int duration = 2 * (Math.max(timeInterval, pollInterval)) + 2;
			Date expiration = new Date(Instant.ofEpochMilli(date.getTime()).plusSeconds(duration * 60).toEpochMilli());
			if (oldMessages != null)
				for (TraffMessage message : oldMessages)
					if (id.equals(message.id)) {
						hasOldMessage = true;
						if (message.receiveTime.before(receiveTime))
							receiveTime = message.receiveTime;
						if (message.expirationTime.after(expiration))
							expiration = message.expirationTime;
					}
			builder.setReceiveTime(receiveTime);
			builder.setExpirationTime(expiration);

			/*
			 * if returnNormal is false, skip messages not indicating abnormal conditions:
			 * speed must be less than 20 km/h and less than 20% below the posted limit
			 * OR traffic type must be something other than "normal" or "heavy"
			 * OR traffic type is "heavy" and vehicle count is at least 750/h
			 */
			if (!returnNormal) {
				boolean isNormal = "normal".equals(segment.trafficType) ? true : false;
				if ("heavy".equals(segment.trafficType) && ((segment.numberOfVehicles * timeInterval / 60) < 750))
					isNormal = true;
				if (isNormal) {
					double threshold = Math.min(segment.summerSpeed, segment.winterSpeed);
					threshold = Math.min(threshold - 20.0, threshold * .8);
					if ((segment.averageSpeed == null) || (segment.averageSpeed >= threshold)) {
						if (hasOldMessage) {
							/* build a cancellation message and add it */
							builder.setCancellation(true);
							res.add(builder.build());
						}
						continue;
					}
				}
			}

			/* start building the location */
			boolean isNegative = "negative".equals(segment.direction);
			TraffLocation.Builder locBuilder = new TraffLocation.Builder();
			locBuilder.setDirectionality(Directionality.ONE_DIRECTION);
			locBuilder.setCountry("LT");
			locBuilder.setRoadRef(roadNr);
			TraffLocation.Point.Builder fromBuilder = new TraffLocation.Point.Builder();
			LatLon fromCoords = Lks94.getWgs84FromLks94(segment.startX, segment.startY);
			fromBuilder.setCoordinates(fromCoords);
			TraffLocation.Point.Builder toBuilder = new TraffLocation.Point.Builder();
			LatLon toCoords = Lks94.getWgs84FromLks94(segment.endX, segment.endY);
			toBuilder.setCoordinates(toCoords);
			if ((x != null) && (y != null)) {
				LatLon xyCoords = new LatLon(x.floatValue(), y.floatValue());
				double fromDist = xyCoords.distanceTo(fromCoords);
				double toDist = xyCoords.distanceTo(toCoords);
				if ((fromDist < toDist) && (fromDist < 1.0)) {
					if (km != null)
						fromBuilder.setDistance(km.floatValue());
					fromBuilder.setJunctionName(name);
				} else if ((fromDist > toDist) && (toDist < 1.0)) {
					if (km != null)
						toBuilder.setDistance(km.floatValue());
					toBuilder.setJunctionName(name);
				} else {
					TraffLocation.Point.Builder viaBuilder = new TraffLocation.Point.Builder();
					viaBuilder.setCoordinates(xyCoords);
					if (km != null)
						viaBuilder.setDistance(km.floatValue());
					viaBuilder.setJunctionName(name);
					TraffLocation.Point via = viaBuilder.build();
					locBuilder.setVia(via);
				}
			}
			TraffLocation.Point from = fromBuilder.build();
			locBuilder.setFrom(from);
			TraffLocation.Point to = toBuilder.build();
			locBuilder.setTo(to);
			if (roadNr != null) {
				if (roadNr.startsWith("A")) {
					/* some A roads are motorways, find out if we are on a motorway section */
					TraffLocation.RoadClass roadClass = null;
					if (motorwayBboxes.containsKey(roadNr)) {
						int matched = 0;
						for (BoundingBox bbox : motorwayBboxes.get(roadNr)) {
							for (TraffLocation.Point point : new TraffLocation.Point[] {from, to})
								if ((point != null) && (bbox.contains(point.coordinates)))
									matched++;
							if (matched > 0)
								break;
						}
						if (matched == 2)
							/* the entire stretch is a motorway */
							roadClass = RoadClass.MOTORWAY;
						else if (matched > 0)
							/* part motorway, part trunk; use the predominant road class */
							roadClass = defaultRoadClassMappings.get(roadNr);
					}
					if (roadClass != null)
						/* set the road class we’ve determined */
						locBuilder.setRoadClass(roadClass);
					else
						/* or default to trunk */
						locBuilder.setRoadClass(RoadClass.TRUNK);
				} else
					if (roadNr.length() <= 3)
						locBuilder.setRoadClass(RoadClass.PRIMARY);
					else
						locBuilder.setRoadClass(RoadClass.OTHER);
			}
			/*
			 * roadName is usually the route, separated by (real!!!) dashes.
			 * 
			 * Destinations abroad are given by their Lithuanian names and followed by an asterisk. We use
			 * them unchanged: for display, this results in a notation Lithuanian audiences would be familiar
			 * with, whereas for navigation, exonyms are useless anyway.
			 * 
			 * We exclude names which contain the word “kelias” in any inflected form, to avoid parsing names
			 * like “Privažiuojamasis kelias prie Patašinės nuo kelio Vilnius–Prienai–Marijampolė” into
			 * destinations “Privažiuojamasis kelias prie Patašinės nuo kelio Vilnius” and “Marijampolė”.
			 */
			if ((roadName != null) && roadName.contains("–") && !roadName.matches(".* kel(ias|io|iui|ią|iu|yje) .*")) {
				locBuilder.setOrigin(roadName.substring(0, roadName.indexOf('–')));
				locBuilder.setDestination(roadName.substring(roadName.lastIndexOf('–') + 1, roadName.length()));
			} else
				// TODO should we set roadName unconditionally?
				locBuilder.setRoadName(roadName);
			if (isNegative)
				locBuilder.invert();
			builder.setLocation(locBuilder.build());

			/* build event */
			TraffEvent.Builder evBuilder = new TraffEvent.Builder();
			evBuilder.setEventClass(TraffEvent.Class.CONGESTION);
			if ("normal".equals(segment.trafficType))
				evBuilder.setType(TraffEvent.Type.CONGESTION_NORMAL_TRAFFIC);
			else if ("heavy".equals(segment.trafficType))
				evBuilder.setType(TraffEvent.Type.CONGESTION_HEAVY_TRAFFIC);
			else if ("slow".equals(segment.trafficType))
				evBuilder.setType(TraffEvent.Type.CONGESTION_SLOW_TRAFFIC);
			else
				evBuilder.setType(TraffEvent.Type.CONGESTION_TRAFFIC_PROBLEM);
			if (segment.averageSpeed != null)
				evBuilder.setSpeed((int)(segment.averageSpeed + .5));
			builder.addEvent(evBuilder.build());

			/* add message */
			res.add(builder.build());
		}
		return res;
	}

	public LtIntensityMessage(Date date, Integer id, String roadNr, Double km, String roadName, Double x,
			Double y, String name, Integer timeInterval, RoadSegment[] roadSegments) {
		super();
		this.date = date;
		this.id = id;
		this.roadNr = roadNr;
		this.km = km;
		this.roadName = roadName;
		this.x = x;
		this.y = y;
		this.name = name;
		this.timeInterval = timeInterval;
		this.roadSegments = roadSegments;
	}
}
