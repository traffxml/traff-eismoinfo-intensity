/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml-eismoinfo-intensity library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.test.eismoinfo;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.traffxml.eismoinfo.trafficintensity.Lks94;
import org.traffxml.eismoinfo.trafficintensity.LtIntensityFeed;
import org.traffxml.eismoinfo.trafficintensity.LtIntensityMessage;
import org.traffxml.eismoinfo.trafficintensity.RoadSegment;
import org.traffxml.traff.LatLon;
import org.traffxml.traff.TraffFeed;
import org.traffxml.viewer.TraffViewer;
import org.traffxml.viewer.input.ConverterSource;
import org.traffxml.viewer.input.StaticSource;

public class IntensityTest {
	private static final IntensityConverter CONVERTER = new IntensityConverter();
	private static final String DEFAULT_URL = "http://old.eismoinfo.lt/traffic-intensity-service";
	private static final int FLAG_CONVERT = 0x1;
	private static final int FLAG_DISSECT = 0x2;
	private static final int FLAG_DUMP = 0x4;
	private static final int FLAG_NORMAL = 0x10000;
	private static final int FLAG_GUI = 0x20000;
	private static final int MASK_OPERATIONS = 0xFFFF;

	private static boolean returnNormal = false;

	private static String getParam(String param, String[] args, int pos) {
		if(pos >= args.length) {
			System.out.println("-" + param + " needs an argument.");
			System.exit(1);
		}
		return args[pos];
	}

	private static void printUsage() {
		System.out.println("Arguments:");
		System.out.println("  -convert <feed>  Convert <feed> to TraFF");
		System.out.println("  -dissect <feed>  Examine <feed> and print results");
		System.out.println("  -dump <feed>     Dump raw feed <feed>");
		System.out.println("  -normal          Generate messages for normal traffic (only with -convert)");
		System.out.println("  -gui             Launch TraFF Viewer UI (not with -dissect or -dump)");
		System.out.println();
		System.out.println("<feed> can refer to a local file or an http/https URL");
	}

	public static void main(String[] args) {
		LtIntensityFeed feed = null;
		String input = null;
		InputStream dataInputStream = null;
		ByteArrayOutputStream dataOutputStream = new ByteArrayOutputStream();
		int i;
		int flags = 0;

		if (args.length == 0) {
			printUsage();
			System.exit(1);
		}
		for (i = 0; i < args.length; i++) {
			if ("-convert".equals(args[i])) {
				input = getParam("convert", args, ++i);
				flags |= FLAG_CONVERT;
			} else if ("-dissect".equals(args[i])) {
				input = getParam("dissect", args, ++i);
				flags |= FLAG_DISSECT;
			} else if ("-dump".equals(args[i])) {
				input = getParam("dump", args, ++i);
				flags |= FLAG_DUMP;
			} else if ("-normal".equals(args[i])) {
				flags |= FLAG_NORMAL;
				returnNormal = true;
			} else if ("-gui".equals(args[i])) {
				flags |= FLAG_GUI;
			} else {
				System.out.println("Unknown argument: " + args[i]);
				System.out.println();
				printUsage();
				System.exit(0);
			}
		}
		if ((flags & (MASK_OPERATIONS | FLAG_GUI)) == 0) {
			System.err.println("An operation or -gui must be specified.");
			System.exit(1);
		}
		if ((flags & (MASK_OPERATIONS | FLAG_GUI)) == FLAG_GUI) {
			TraffViewer.launch(CONVERTER, DEFAULT_URL);
		} else if ((flags & MASK_OPERATIONS) != 0) {
			if (input.startsWith("http://") || input.startsWith("https://")) {
				URL url;
				try {
					url = new URL(input);
					if ((flags & FLAG_DUMP) == 0)
						feed = CONVERTER.parse(url);
					else
						dataInputStream = url.openStream();
				} catch (MalformedURLException e) {
					System.err.println("Input URL is invalid. Aborting.");
					System.exit(1);
				} catch (IOException e) {
					System.err.println("Cannot read from input URL. Aborting.");
					System.exit(1);
				}
			} else {
				File inputFile = new File(input);
				try {
					if ((flags & FLAG_DUMP) == 0)
						feed = CONVERTER.parse(inputFile);
					else
						dataInputStream = new FileInputStream(inputFile);
				} catch (FileNotFoundException e) {
					System.err.println("Input file does not exist. Aborting.");
					System.exit(1);
				} catch (IllegalArgumentException e) {
					System.err.println(String.format("%s. Aborting.", e.getMessage()));
					System.exit(1);
				}
			}
		}
		if ((feed != null) || (dataInputStream != null)) {
			switch(flags & MASK_OPERATIONS) {
			case FLAG_CONVERT:
				convertFeed(feed, ((flags & FLAG_NORMAL) != 0), ((flags & FLAG_GUI) != 0));
				break;
			case FLAG_DISSECT:
				dissectFeed(feed);
				break;
			case FLAG_DUMP:
				try {
					i = dataInputStream.read();
					while (i != -1) {
						if (i >= 32) dataOutputStream.write(i);
						i = dataInputStream.read();
					}
					dataInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
					System.exit(1);
				}
				dump(dataOutputStream);
				break;
			default:
				System.err.println("Invalid combination of options. Aborting.");
				System.exit(1);
			}
		}
	}

	static void convertFeed(LtIntensityFeed feed, boolean returnNormal, boolean gui) {
		try {
			TraffFeed tFeed = CONVERTER.convert(feed);
			if (gui) {
				StaticSource.readFeed(tFeed);
				TraffViewer.launch(CONVERTER, DEFAULT_URL);
			} else
				System.out.println(tFeed.toXml());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static void dissectFeed(LtIntensityFeed feed) {
		Set<Integer> ids = new HashSet<Integer>();
		Set<String> trafficTypes = new HashSet<String>();
		for (LtIntensityMessage message : feed.messages) {
			dissectMessage(message);
			ids.add(message.id);
			if (message.roadSegments != null)
				for (RoadSegment segment : message.roadSegments)
					trafficTypes.add(segment.trafficType);
		}
		System.out.println(String.format("Messages in feed: %d", feed.messages.length));
		System.out.println(String.format("Unique IDs:       %d", ids.size()));
		System.out.println(String.format("Traffic types:    %d", trafficTypes.size()));
		for (String trafficType : trafficTypes)
			System.out.println(String.format("  %s", trafficType));
	}

	static void dissectMessage(LtIntensityMessage message) {
		System.out.println(String.format("Message: %d", message.id));
		System.out.println(String.format("  Date: %s, interval %d", message.date, message.timeInterval));

		String route = "";
		if ((message.roadName != null) && message.roadName.contains("–")) {
			String origin = message.roadName.substring(0, message.roadName.indexOf('–'));
			String destination = message.roadName.substring(message.roadName.lastIndexOf('–') + 1, message.roadName.length());
			route = String.format(" (%s → %s)", origin, destination);
		}
		System.out.println(String.format("  Road: %s %s%s", message.roadNr, message.roadName, route));

		System.out.println(String.format("  At: km %.3f (%s)", message.km, message.name));
		System.out.println(String.format("  Coords: %1$.6f, %2$.6f (https://www.openstreetmap.org/?mlat=%1$f&mlon=%2$f#map=13/%1$f/%2$f)",
				message.x, message.y));
		if (message.roadSegments == null) {
			System.out.println("  Road segments: null");
		} else {
			System.out.println(String.format("  Road segments: %d", message.roadSegments.length));
			for (RoadSegment segment : message.roadSegments)
				dissectRoadSegment(segment);
		}
	}

	static void dissectRoadSegment(RoadSegment segment) {
		System.out.println(String.format("    Direction: %s", segment.direction));
		System.out.println(String.format("      Coords: (%.2f, %.2f)–(%.2f, %.2f)",
				segment.startX, segment.startY, segment.endX, segment.endY));

		LatLon start = Lks94.getWgs84FromLks94(segment.startX, segment.startY);
		LatLon end = Lks94.getWgs84FromLks94(segment.endX, segment.endY);
		System.out.println(String.format("      WGS84: (%.5f, %.5f)–(%.5f, %.5f)",
				start.lat, start.lon, end.lat, end.lon));
		if ("negative".equals(segment.direction)) {
			System.out.println(String.format("      OSM: https://www.openstreetmap.org/directions?engine=fossgis_osrm_car&route=%.6f%%2C%.6f%%3B%.6f%%2C%.6f#map=13/%.6f/%.6f",
					end.lat, end.lon, start.lat, start.lon, (end.lat + start.lat)/ 2.0f, (end.lon + start.lon)/ 2.0f));
			System.out.println(String.format("      Bearing: %.0f",
					end.bearingTo(start)));
		} else {
			System.out.println(String.format("      OSM: https://www.openstreetmap.org/directions?engine=fossgis_osrm_car&route=%.6f%%2C%.6f%%3B%.6f%%2C%.6f#map=13/%.6f/%.6f",
					start.lat, start.lon, end.lat, end.lon, (start.lat + end.lat)/ 2.0f, (start.lon + end.lon)/ 2.0f));
			System.out.println(String.format("      Bearing: %.0f",
					start.bearingTo(end)));
		}

		System.out.println(String.format("      Speed: %d (summer), %d (winter)",
				segment.summerSpeed, segment.winterSpeed));
		System.out.println(String.format("      Traffic: %s, %d vehicles, average speed %.2f",
				segment.trafficType, segment.numberOfVehicles, segment.averageSpeed));
	}

	private static void dump(ByteArrayOutputStream stream) {
		JSONTokener tokener = new JSONTokener(stream.toString());
		while(true) {
			Object object = null;
			try {
				object = tokener.nextValue();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			if (object == null)
				break;
			parse(null, object, 0);
		}
		System.out.println("Done.");
	}

	private static String padding(int level) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < level; i++)
			builder.append(("  "));
		return builder.toString();
	}

	private static void parse(String key, Object object, int level) {
		if (object instanceof JSONObject) {
			parse(key, (JSONObject) object, level);
		} else if (object instanceof JSONArray) {
			parse(key, (JSONArray) object, level);
		} else if ((object instanceof String) || (object instanceof Boolean)
				|| (object instanceof Integer) || (object instanceof Long)
				|| (object instanceof Double)) {
			parse(key, object.getClass(), object.toString(), level);
		} else
			System.out.println(String.format("%s%s: (%s)", padding(level), key, object.getClass().getName()));
	}

	private static void parse(String key, JSONObject object, int level) {
		//System.out.println("JSONObject: " + ((JSONObject) object).toString());
		System.out.println(String.format("%s%s: (JSONObject)", padding(level), key));
		Iterator<String> keys = object.keys();
		while (keys.hasNext()) {
			String chKey = keys.next();
			Object value = object.get(chKey);
			parse(chKey, value, level + 1);
		}
	};

	private static void parse(String key, JSONArray array, int level) {
		System.out.println(String.format("%s%s: (JSONArray)", padding(level), key));
		for (int j = 0; j < array.length(); j++) {
			Object object = array.get(j);
			parse(key, object, level + 1);
		}
	};

	private static void parse(String key, Class<? extends Object> clazz, String string, int level) {
		System.out.println(String.format("%s%s: (%s) %s", padding(level), key, clazz.getSimpleName(), string));
	}

	private static class IntensityConverter implements ConverterSource {

		@Override
		public TraffFeed convert(File file) throws Exception {
			LtIntensityFeed feed = parse(file);
			return convert(feed);
		}

		@Override
		public TraffFeed convert(URL url) throws Exception {
			LtIntensityFeed feed = parse(url);
			return convert(feed);
		}

		@Override
		public TraffFeed convert(Properties properties) throws Exception {
			throw new UnsupportedOperationException("This operation is not implemented yet");
		}

		private TraffFeed convert(LtIntensityFeed feed) {
			return new TraffFeed(feed.toTraff("test", null, 0, returnNormal));
		}

		private LtIntensityFeed parse(File file) throws FileNotFoundException {
			if (!file.exists()) {
				throw new IllegalArgumentException("Input file does not exist");
			} else if (!file.isFile()) {
				throw new IllegalArgumentException("Input file is not a file");
			} else if (!file.canRead()) {
				throw new IllegalArgumentException("Input file is not readable");
			}
			return LtIntensityFeed.parseJson(new FileInputStream(file));
		}

		private LtIntensityFeed parse(URL url) throws IOException {
			return LtIntensityFeed.parseJson(url.openStream());
		}
	}
}
