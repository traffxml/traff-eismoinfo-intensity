[![Release](https://jitpack.io/v/org.traffxml/traff-eismoinfo-intensity.svg)](https://jitpack.io/#org.traffxml/traff-eismoinfo-intensity)

Javadoc for `master` is at https://traffxml.gitlab.io/traff-eismoinfo-intensity/javadoc/master/.

Javadoc for `dev` is at https://traffxml.gitlab.io/traff-eismoinfo-intensity/javadoc/dev/.
